import 'package:flutter/material.dart';
import 'package:workio_flutter/utils/colors.dart';
import 'package:workio_flutter/utils/responsive_screen.dart';
class SelectDateWidget extends StatefulWidget {
  
  final Color focusBorderColor;

  const SelectDateWidget({Key key, this.focusBorderColor}) : super(key:key);
  
  @override
  _SelectDateWidgetState createState() => _SelectDateWidgetState();
}

class _SelectDateWidgetState extends State<SelectDateWidget> {

  String _fecha;
  
  TextEditingController _fechaNacimientoController = TextEditingController();

  @override
  void dispose(){
    super.dispose();
    _fechaNacimientoController.dispose();
  }

  Screen size;
  double widht;
  double height;

  @override
  Widget build(BuildContext context){
    widht = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;
    size = Screen(MediaQuery.of(context).size);
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Container(
              margin: EdgeInsets.only(top: height / 400),
              padding: EdgeInsets.all(size.getWidthPx(0)),
              alignment: Alignment.center,
              height: size.getWidthPx(40),
              decoration: BoxDecoration(
                color: Colors.grey.shade100,
                border: Border.all(color: widget.focusBorderColor??Colors.grey.shade400, width: 1.0),
                borderRadius: BorderRadius.circular(8.0),
              ),
              child: TextField(
                obscureText: false,
                controller: _fechaNacimientoController,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: 'Fecha de nacmiento',
                  prefixIcon: Icon(Icons.calendar_today,
                  color: colorIcons, 
                  size: size.getWidthPx(22)
                ),
              ),
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
                _selectDate(context);
              },
            ),
          )),
        ],
      ),
      padding: EdgeInsets.only(bottom :size.getWidthPx(8)),
      margin: EdgeInsets.only(top: size.getWidthPx(8), right: size.getWidthPx(8), left:size.getWidthPx(8)),
    );
  }

  _selectDate(BuildContext context) async {
    DateTime picked = await showDatePicker(
        context: context,
        initialDate: new DateTime.now(),
        firstDate: new DateTime(1960),
        lastDate: new DateTime(2025),
        locale: Locale('es', 'ES'));
    if (picked != null) {
      setState(() {
        _fecha =
            "${picked.year.toString()}-${picked.month.toString().padLeft(2, '0')}-${picked.day.toString().padLeft(2, '0')}";
        _fechaNacimientoController.text = _fecha;
      });
    }
  }
}