import 'package:flutter/material.dart';
import 'package:workio_flutter/UI/detallesTrab_page.dart';

class CardTrabajosWidget extends StatefulWidget {
  
  final String titulo;
  final String descripcion;
  final int idTrab;

  const CardTrabajosWidget(this.titulo, this.descripcion, this.idTrab);
  
  @override
  _CardTrabajosState createState() => _CardTrabajosState();
}

class _CardTrabajosState extends State<CardTrabajosWidget> {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      child: Column(children: <Widget>[
        ListTile(
          //leading: _cardImg(),
          title: Text(widget.titulo),
          subtitle: Text(widget.descripcion),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            FlatButton(
              child: Text('Ver'),
              onPressed: () {
                Navigator.push(context, 
                  MaterialPageRoute(builder: (context) => DetallesTrabPage(widget.idTrab)));
              },
            ),
          ],
        )
      ]),
    );
  }
}