import 'dart:io';

import 'package:flutter/material.dart';

class FotoPerfilWidget extends StatefulWidget {
  
  final File foto;

  const FotoPerfilWidget(this.foto);
  
  @override
  _FotoPerfilWidgetState createState() => _FotoPerfilWidgetState();
}

class _FotoPerfilWidgetState extends State<FotoPerfilWidget> {
  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      radius: 80,
      child: ClipOval(
        child: SizedBox(
          width: 200.0,
          height: 200.0,
          child: (widget.foto != null)
              ? Image.file(
                  widget.foto,
                  fit: BoxFit.fill,
                  // ):(_persona.fotoUrl!=null)?Image.network(
                  //   _persona.fotoUrl,
                  //   fit: BoxFit.fill,
                )
              : Image.asset(
                  'assets/non_user.png',
                  fit: BoxFit.fill,
                ),
        ),
      ),
    );
  }
}