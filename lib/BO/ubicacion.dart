class Ubicacion{

  int id;
  double longitud;
  double latitud;
  String direccion;
  String referencias;

  Ubicacion();

  Ubicacion.fromJson(Map<String, dynamic>json)
    : id           = json['id'],
      longitud     = json['longitud'],
      latitud      = json['latitud'], 
      direccion    = json['direccion'],
      referencias  = json['referencias']
    ;
      
  static String get jsonString => null;
  
  Map<String, dynamic> toJson()=>
  {
    'id'          : id,
    'longitud'    : longitud,
    'latitud'     : latitud,
    'direccion'   : direccion,
    'referencias' : referencias,
  };
}