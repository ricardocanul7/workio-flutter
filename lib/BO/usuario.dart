class Usuario{

  int id;
  String username;
  String email;
  String contrasenia;
  String fechaRegistro;
  int estatusId; //Activo:1-Revision:2-Bloqueado:3-Desactivado:4
  int personaId;
  int perfilId; //Empleador:2-Trabajador:3

  Usuario();

  Usuario.fromJson(Map<String, dynamic>json)
    : id            = json['id'],
      username      = json['username'],
      email         = json['email'],
      contrasenia   = json['contrasenia'],
      fechaRegistro = json['fechaRegistro'],
      estatusId     = json['estatusId'],
      personaId     = json['personaId'],
      perfilId      = json['perfilId'];

  static String get jsonString => null;
  
  Map<String, dynamic> toJson()=>
  {
    'username'    : username,
    'email'       : email,
    'contrasenia' : contrasenia,
    'fechaRegistro': fechaRegistro,
    'estatusId'   : estatusId,
    'personaId'   : personaId,
    'perfilId'    : perfilId,
  };
}
