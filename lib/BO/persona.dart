class Persona{

  int id;
  String nombres;
  String apellidoP;
  String apellidoM;
  String fechaNacimiento;
  String fotoUrl;
  String direccion;
  String telefono;
  String celular;

  Persona();

  Persona.fromJson(Map<String, dynamic>json)
    : 
      id              = json['id'],
      nombres         = json['nombres'],
      apellidoP       = json['apellidoP'],
      apellidoM       = json['apellidoM'],
      fechaNacimiento = json['fechaNacimiento'],
      fotoUrl         = json['fotoUrl'],
      direccion       = json['direccion'],
      telefono        = json['telefono'],
      celular         = json['celular'];

  static String get jsonString => null;
  
  Map<String, dynamic> toJson()=>
  {
    'nombres'         : nombres,
    'apellidoP'       : apellidoP,
    'apellidoM'       : apellidoM,
    'fechaNacimiento' : fechaNacimiento,
    'fotoUrl'         : fotoUrl,
    'direccion'       : direccion,
    'telefono'        : telefono,
    'celular'         : celular,
  };


}