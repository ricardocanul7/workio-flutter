class Pago{

  int id;
  String descripcion;

  Pago();

  Pago.fromJson(Map<String, dynamic>json)
    : id               = json['id'] as int,
      descripcion      = json['descripcion']
    ;
      
  static String get jsonString => null;
  
  Map<String, dynamic> toJson()=>
  {
    'id'               : id,
    'descripcion'      : descripcion,
  };
}