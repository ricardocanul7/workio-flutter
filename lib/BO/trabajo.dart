class Trabajo{

  int id;
  String titulo;
  String descripcion;
  int categoriaId;
  int personaOrigenId;
  String fechaPublicacion;
  //final DateTime fechaCompletado;
  int ubicacionId;
  int estadoId; 
  // 1 PENDIENTE DE APROBACIÓN, 2 APROBADO_PUBLICADO, 3 EN PROCESO DE REALIZACIÓN, 4 COMPLETADO
  int personaOrigen;

  Trabajo();

  Trabajo.fromJson(Map<String, dynamic>json)
    : id               = json['id'],
      titulo           = json['titulo'],
      descripcion      = json['descripcion'],
      categoriaId      = json['categoriaId'],
      personaOrigenId  = json['personaOrigenId'],
      fechaPublicacion = json['fechaPublicacion'],
      //fechaCompletado  = DateTime.tryParse(json['fechaCompletado']),
      ubicacionId      = json['ubicacionId'],
      estadoId         = json['estadoId'],
      personaOrigen    = json['personaOrigen'];
      
  static String get jsonString => null;
  
  Map<String, dynamic> toJson()=>
  {
    'id'               : id,
    'titulo'           : titulo,
    'descripcion'      : descripcion,
    'categoriaId'      : categoriaId,
    'personaOrigenId'  : personaOrigenId,
    'fechaPublicacion' : fechaPublicacion,
    //'fechaCompletado'  : fechaCompletado,
    'ubicacionId'      : ubicacionId,
    'estadoId'         : estadoId,
    'personaOrigen'    : personaOrigen,
  };

}