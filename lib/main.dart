import 'package:flutter/material.dart';
import 'package:workio_flutter/BO/usuario.dart';
import 'package:workio_flutter/UI/home_page.dart';
import 'package:workio_flutter/UI/login.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  Usuario _usuario;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'WorkIO',
      debugShowCheckedModeBanner: false,
      localizationsDelegates:[
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
      supportedLocales: [
        const Locale('es', 'ES'),
        const Locale('en', 'US'),
      ],

      initialRoute: 'Log',
      routes: <String, WidgetBuilder>{
        'Log' : (BuildContext context) => LoginPage(),

      },
      onGenerateRoute: ( settings ){

        return MaterialPageRoute(
          builder: ( BuildContext context ) => HomePage(_usuario.personaId, _usuario.perfilId)
        );
      }, 
    );
  }
}

