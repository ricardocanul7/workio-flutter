import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:workio_flutter/UI/chat_page.dart';
import 'package:workio_flutter/UI/misPublicaciones_page.dart';
import 'package:workio_flutter/UI/perfil_page.dart';
import 'package:workio_flutter/UI/trabajos_page.dart';

class HomePage extends StatefulWidget {
  final int idPersona;
  final int idPerfil;

  const HomePage(this.idPersona, this.idPerfil);


  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  

  int currentIndex =0;

  @override
  Widget build(BuildContext context) {

    print('El id de la persona es: ${widget.idPersona}');
    print('El idPerfil del usuario: ${widget.idPerfil}');

    double _size = 25;

    return Scaffold(
      // appBar: AppBar(
      //   // title: Text('${UserLogged.usuario.username}')
      //   title: Text('WorkIO'),
      //   leading: new Container(),
      // ),
      bottomNavigationBar: CurvedNavigationBar(
        
        height: 55,
        backgroundColor: Colors.white,
        color: Colors.blueAccent,
        
        
        index: currentIndex ,
        onTap: (index) {
          //Handle button tap
          setState(() {
            currentIndex = index;
          });
        },

        items: <Widget>[
          Icon(Icons.store_mall_directory,size: _size,),
          Icon(Icons.home, size:  _size),
          Icon(Icons.person_pin, size: _size),
          Icon(Icons.message, size: _size),
        ],
        
      ),
      body: _callPage(currentIndex)
    );
  }

  Widget _callPage(int paginaActual){
    switch(paginaActual){
      case 0: return MisPublicacionesPage(widget.idPersona, widget.idPerfil);
      case 1: return TrabajosPage();
      case 2: return PerfilPage();
      case 3: return ChatPage();

      default:
        return TrabajosPage();
    }
  }
}
