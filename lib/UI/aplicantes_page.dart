import 'package:flutter/material.dart';
import 'package:workio_flutter/utils/responsive_screen.dart';
import 'package:workio_flutter/utils/utils.dart';
import 'package:workio_flutter/widgets/widgets.dart';

class AplicantesPage extends StatefulWidget {
  @override
  _AplicantesPageState createState() => _AplicantesPageState();
}

class _AplicantesPageState extends State<AplicantesPage> {
  Screen size;
  double widht;
  double height;

  TextEditingController _descripcionController = TextEditingController();

  @override
  void dispose(){
    super.dispose();
    _descripcionController.dispose();
  }
  

  @override
  Widget build(BuildContext context) {
    size = Screen(MediaQuery.of(context).size);
    height = MediaQuery.of(context).size.height;
    widht = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: backgroundColor,
      resizeToAvoidBottomInset: true,
      body: Stack(children: <Widget>[
        ClipPath(
          clipper: BottomShapeClipper(),
          child: Container(
            color: colorCurve,
          )
        ),
        SingleChildScrollView(
          child: SafeArea(
            top: true,
            bottom: false,
            child: Container(
              margin: EdgeInsets.symmetric(
                horizontal: size.getWidthPx(20),
                vertical: size.getWidthPx(20)
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _aplicantesGradientText(),
                  SizedBox(height: size.getWidthPx(25)),
                ]
              ),
            ),
          ),
        )
      ])
    );
  }

  GradientText _aplicantesGradientText() {
    return GradientText('Lista de aplicantes',
        gradient: LinearGradient(colors: [Colors.blue[600], Colors.teal[300]]),
        style: TextStyle(
            fontFamily: 'Exo2', fontSize: 25, fontWeight: FontWeight.bold));
  }
}