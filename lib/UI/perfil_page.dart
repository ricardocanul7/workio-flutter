import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:workio_flutter/utils/responsive_screen.dart';
import 'package:workio_flutter/utils/utils.dart';
import 'package:workio_flutter/widgets/foto_perfil.dart';
import 'package:workio_flutter/widgets/gradient_text.dart';

class PerfilPage extends StatefulWidget {
  @override
  _PerfilPageState createState() => _PerfilPageState();
}

class _PerfilPageState extends State<PerfilPage> {

  File foto;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Screen size;
  double widht;
  double height;
  
  @override
  Widget build(BuildContext context) {
    size = Screen(MediaQuery.of(context).size);
    height = MediaQuery.of(context).size.height;
    widht = MediaQuery.of(context).size.height;
    
    return Center(
      
      child: SafeArea(
        child: ListView(padding: EdgeInsets.all(10.0), 
          children: <Widget>[
            _trabajosGradientText(),
            SizedBox(height: 20),
            registerFields(),
          ]
        ),
      ), 
    );
  }

  GradientText _trabajosGradientText() {
    return GradientText('Mi perfil',
        gradient: LinearGradient(colors: [Colors.blue[600], Colors.teal[300]]),
        style: TextStyle(
            fontFamily: 'Exo2', fontSize: 25, fontWeight: FontWeight.bold));
  }

  FotoPerfilWidget _fotoPerfil(){
    return FotoPerfilWidget(foto);
  }

  Widget _botonFoto() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        SizedBox(width: size.getWidthPx(0)),
        IconButton(
          icon: Icon(Icons.add_a_photo, color: colorIcons),
          onPressed: _tomarFoto,
        ),
        FlatButton(
          onPressed: (){}, 
          child: Text("Subir"),
          color: Colors.black ,
        ),
        // SizedBox(width: size.getWidthPx(90)),
        IconButton(
          icon: Icon(Icons.add_photo_alternate, color: colorIcons),
          onPressed: _seleccionarFoto,
        ),
      ],
    );
  }

  registerFields() => Container(
    child: Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          _fotoPerfil(),
          _botonFoto(),
          //_confirmPasswordWidget(),
        ],
      )
    ),
  );

  _seleccionarFoto() async {
    _procesarImagen(ImageSource.gallery);
  }

  _tomarFoto() async {
    _procesarImagen(ImageSource.camera);
  }

  _procesarImagen(ImageSource origen) async {
    foto = await ImagePicker.pickImage(source: origen);

    if (foto != null) {
      //Limpieza

    }
    setState(() {});
  }

  // _uploadImg() async {
  //   String _url;

  //   //String username = _usernameController.text;

  //   StorageReference reference = FirebaseStorage.instance
  //       .ref()
  //       .child("foto_perfil/${}" + ".jpg");
  //   StorageUploadTask uploadTask = reference.putFile(foto);
  //   StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;
  //   var downURL = await (taskSnapshot).ref.getDownloadURL();
  //   _url = downURL;
  //   print('La foto se subió');
  //   print(_url);
  //   return _url;
  // }

}