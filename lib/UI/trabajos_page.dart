import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';
import 'package:workio_flutter/BO/trabajo.dart';
import 'package:workio_flutter/UI/detallesTrab_page.dart';
import 'package:workio_flutter/widgets/card_trabajos.dart';
import 'package:workio_flutter/widgets/gradient_text.dart';

class TrabajosPage extends StatefulWidget {
  @override
  _TrabajosPageState createState() => _TrabajosPageState();
}

class _TrabajosPageState extends State<TrabajosPage> {

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SafeArea(
        child: ListView(
          padding: EdgeInsets.all(10.0), 
          children: <Widget>[
            _trabajosGradientText(),
            SizedBox(height: 20),
            Container(
              child: FutureBuilder(
                future: _cargarTrabajos(),
                builder: (BuildContext context, AsyncSnapshot snapshot){
                  if(snapshot.data == null){
                    return Container(child: Center(child: Text("Cargando..."),),);
                  }else{
                    return ListView.builder(
                      shrinkWrap: true,
                      physics: ScrollPhysics(),
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, int index){
                      return _cardTrabajos(snapshot.data[index].titulo, snapshot.data[index].descripcion, snapshot.data[index].id);
                      },
                    );
                  }
                },
              ),
            ),
          ]
        ),
      ), 
    );
  }

  GradientText _trabajosGradientText() {
    return GradientText('Publicaciones de trabajos',
        gradient: LinearGradient(colors: [Colors.blue[600], Colors.teal[300]]),
        style: TextStyle(
            fontFamily: 'Exo2', fontSize: 25, fontWeight: FontWeight.bold));
  }

  CardTrabajosWidget _cardTrabajos(String titulo, String descripcion, int idTrab){
    return CardTrabajosWidget(titulo, descripcion, idTrab);
  }

  // Widget _cardTrabajos(BuildContext context, String titulo, String descripcion, int idTrab) {
  //   return Card(
  //     elevation: 5,
  //     child: Column(children: <Widget>[
  //       ListTile(
  //         //leading: _cardImg(),
  //         title: Text(titulo),
  //         subtitle: Text(descripcion),
  //       ),
  //       Row(
  //         mainAxisAlignment: MainAxisAlignment.end,
  //         children: <Widget>[
  //           FlatButton(
  //             child: Text('Ver'),
  //             onPressed: () {
  //               Navigator.push(context, 
  //                 MaterialPageRoute(builder: (context) => DetallesTrabPage(idTrab)));
  //             },
  //           ),
  //         ],
  //       )
  //     ]),
  //   );
  // }

  Future<List<Trabajo>> _cargarTrabajos() async{
    
    int pagina = 0;
    int cantTrabajos = 5;

    final resp = await http.get(
      'http://www.workio.somee.com/api/PublicacionesService/GetLastPublicaciones?pageNumber=$pagina&range=$cantTrabajos'
    );

    //print(resp.statusCode);
    List<Trabajo> trabajos = [];

    if(resp.statusCode == 200){
      //Map trabajosMap = jsonDecode(resp.body);
      //var trabajos = Trabajo.fromJson(trabajosMap);
      var jsonData = json.decode(resp.body);
      

      for(var item in jsonData){
        Trabajo trabajo = Trabajo.fromJson(item);
        trabajos.add(trabajo);
      }

      print("Cantidad de trabajos: "+trabajos.length.toString());
      return trabajos;
      //print('El id del trabajo es: ${trabajo.id}');
      //print(resp.body.toString());
    }else{
      return null;
    }
  }
}