import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:workio_flutter/BO/categoria.dart';
import 'package:workio_flutter/BO/pago.dart';
import 'package:workio_flutter/BO/trabajo.dart';
import 'package:workio_flutter/BO/ubicacion.dart';
import 'package:workio_flutter/UI/misPublicaciones_page.dart';
//import 'package:workio_flutter/UI/mapa_page.dart';
import 'package:workio_flutter/utils/colors.dart';
import 'package:workio_flutter/utils/responsive_screen.dart';
import 'package:workio_flutter/widgets/bottom_curve_painter.dart';
import 'package:workio_flutter/widgets/boxfield.dart';
import 'package:workio_flutter/widgets/gradient_text.dart';

class PublicarTrabPage extends StatefulWidget {
  
  final Color focusBorderColor;
  final int idPersona;
  final int idPerfil;

  const PublicarTrabPage(this.idPersona, this.idPerfil, {Key key, this.focusBorderColor}) :super(key:key);
  
  @override
  _PublicarTrabPageState createState() => _PublicarTrabPageState();
}

class _PublicarTrabPageState extends State<PublicarTrabPage> {

  Trabajo _trabajo = Trabajo();
  Ubicacion _ubicacion = Ubicacion();

  String _categoriaSelect;

  var _idCat;

  //var listCat =  _cargarCategoria();

  //List<String> _categorias = [];
  
  //La lista que intento llenar y mostrar
  List<Categoria> categorias = [];


  TextEditingController _tituloController = TextEditingController();
  TextEditingController _descripcionController = TextEditingController();
  TextEditingController _ubicacionController = TextEditingController();
  TextEditingController _referenciaController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _cargarCategoria();
  }

  @override
  void dispose(){
    super.dispose();
    _tituloController.dispose();
    _descripcionController.dispose();
    _ubicacionController.dispose();
  }
  
  Screen size;
  double widht;
  double height;

  @override
  Widget build(BuildContext context) {

    print('Recepción de id: ${widget.idPersona}');
    size = Screen(MediaQuery.of(context).size);
    height = MediaQuery.of(context).size.height;
    widht = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: backgroundColor,
      resizeToAvoidBottomInset: true,
      body: Stack(children: <Widget>[
        ClipPath(
          clipper: BottomShapeClipper(),
          child: Container(
            color: colorCurve,
          )
        ),
        SingleChildScrollView(
          child: SafeArea(
            top: true,
            bottom: false,
            child: Container(
              margin: EdgeInsets.symmetric(
                horizontal: size.getWidthPx(20),
                vertical: size.getWidthPx(20)
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _gradientText(),
                  SizedBox(height: size.getWidthPx(30),),
                  registerFields()
                ]
              ),
            ),
          ),
        )
      ])
    );
  }

  GradientText _gradientText() {
    return GradientText('Publicar trabajo',
        gradient: LinearGradient(colors: [Colors.blue[600], Colors.teal[300]]),
        style: TextStyle(
            fontFamily: 'Exo2', fontSize: 25, fontWeight: FontWeight.bold));
  }

  BoxField _tituloWidget() {
    return BoxField(
        controller: _tituloController,
        hintText: "Título",
        lableText: "title",
        obscureText: false,
        icon: Icons.title,
        iconColor: colorIcons);
  }

  Widget _descripcionWidget(){
    widht = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;
    size = Screen(MediaQuery.of(context).size);
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Container(
              margin: EdgeInsets.only(top: height / 400),
              padding: EdgeInsets.all(size.getWidthPx(0)),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Colors.grey.shade100,
                border: Border.all(color: widget.focusBorderColor??Colors.grey.shade400, width: 1.0),
                borderRadius: BorderRadius.circular(8.0),
              ),
              child: TextField(
                maxLines: null,
                obscureText: false,
                controller: _descripcionController,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: 'Descripción del problema',
                  
                  prefixIcon: Icon(Icons.text_fields,
                  color: colorIcons, 
                  size: size.getWidthPx(22)
                ),
              ),
            ),
          )),
        ],
      ),
      padding: EdgeInsets.only(bottom :size.getWidthPx(8)),
      margin: EdgeInsets.only(top: size.getWidthPx(8), right: size.getWidthPx(8), left:size.getWidthPx(8)),
    );
  }

  BoxField _ubicacionWidget() {
    return BoxField(
        controller: _ubicacionController,
        hintText: "Ubicación",
        lableText: "title",
        obscureText: false,
        icon: Icons.location_on,
        iconColor: colorIcons
    );
  }

  Widget _referenciaWidget(){
    widht = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;
    size = Screen(MediaQuery.of(context).size);
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Container(
              margin: EdgeInsets.only(top: height / 400),
              padding: EdgeInsets.all(size.getWidthPx(0)),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Colors.grey.shade100,
                border: Border.all(color: widget.focusBorderColor??Colors.grey.shade400, width: 1.0),
                borderRadius: BorderRadius.circular(8.0),
              ),
              child: TextField(
                maxLines: null,
                obscureText: false,
                controller: _referenciaController,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: 'Referencías de la ubicación',
                  prefixIcon: Icon(Icons.home,
                  color: colorIcons, 
                  size: size.getWidthPx(22)
                ),
              ),
            ),
          )),
        ],
      ),
      padding: EdgeInsets.only(bottom :size.getWidthPx(8)),
      margin: EdgeInsets.only(top: size.getWidthPx(8), right: size.getWidthPx(8), left:size.getWidthPx(8)),
    );
  }

  Future _cargarUbicacion() async {
    // _ubicacion.longitud = null;
    // _ubicacion.latitud  = null;
    // _ubicacion.direccion = _ubicacionController.text;
    // _ubicacion.referencias = _referenciaController.text;

    Map<String, dynamic> _ubicacion()=>
    {
      'longitud'   : null,
      'latitud'    : null,
      'direccion'  : _ubicacionController.text,
      'referencias': _referenciaController.text
    };

    final resp = await http.post(
      'http://www.workio.somee.com/api/Personas',
      body: jsonEncode(_ubicacion()),
      headers: {"content-Type": "application/json"},
    );
    
    print("Estatus de post ubicación: ${resp.statusCode}");
    print("Cuerpo de post ubicación ${resp.body}");

    Map ubicacionMap = jsonDecode(resp.body);
    var ubicacion = Ubicacion.fromJson(ubicacionMap);

    print("Id de la ubicación: ${ubicacion.id}");
    print('${resp.statusCode}');
    if(resp.statusCode==201){
      _publicarTrabajo(ubicacion.id);
    }

  }

  // RichText _cargarUbicacion() {
  //   return RichText(
  //     text: TextSpan(
  //       style: TextStyle(color: colorCurve),
  //       text: 'Cargar ubicación',
  //       recognizer: TapGestureRecognizer()
  //       //..onTap = () => Navigator.push(context, MaterialPageRoute(builder: (context) => MapaPage()))
  //     )
  //   );
  // }

//Con esto debo llenar el drop
  
  List<DropdownMenuItem<String>> getCategorias(){
    
    List<DropdownMenuItem<String>> lista = new List();

    categorias.forEach((categoria)  {
      lista.add(DropdownMenuItem(
        child: Text(categoria.descripcion),
        value: categoria.id.toString(),
      ));
    });
    
    return lista;
  }

  //MÉTDODO --POST-- PARA PUBLICAR EL TRABAJO
   Future _publicarTrabajo(ubicacionId)async{
    _trabajo.titulo =_tituloController.text;
    _trabajo.descripcion = _descripcionController.text;
    _trabajo.categoriaId = _idCat;
    _trabajo.personaOrigenId = widget.idPersona; //id de la persona que inicio sesión.
    _trabajo.fechaPublicacion = await _getDate();
    _trabajo.ubicacionId = ubicacionId;
    _trabajo.estadoId = 2; //prueba

    print(jsonEncode(_trabajo));

    final resp = await http.post(
      'http://workio.somee.com/api/PublicacionTrabajos',
      body: jsonEncode(_trabajo),
      headers: {"content-Type": "application/json"},
    );

    print("Codigo de respuesta: ${resp.statusCode}");
    print("Cuerpo de respuesta: ${resp.body}");

    if(resp.statusCode == 201){
      print("Publicación de trabajo exitosa");
      
    }
  }

  Future<String> _getDate() async{
    String _today;
   _today = '${DateTime.now().year.toString()}-${DateTime.now().month.toString().padLeft(2, '0')}-${DateTime.now().day.toString().padLeft(2, '0')}';
   print('La fecha de hoy es: $_today');
    return _today;
  }


  Widget _aplicarButtonWidget() {
    return Center(
      child: Container(
        
        padding: EdgeInsets.symmetric(
          vertical: size.getWidthPx(20), horizontal: size.getWidthPx(16)
        ),
        width: size.getWidthPx(200),
        child: RaisedButton(
          elevation: 3.0,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
          padding: EdgeInsets.all(size.getWidthPx(12)),
          child: Text(
            "PUBLICAR",
            style: TextStyle(
                fontFamily: 'Exo2', color: Colors.white, fontSize: 20.0),
          ),
          color: Colors.teal,
          onPressed: ()  {
            _cargarUbicacion();
            // Navigator.push(context, 
            //   MaterialPageRoute(builder: (context)=> AplicarTrabPage()));
            // Going to DashBoard
            //Navigator.push(context, MaterialPageRoute(builder: (context)=>HomePage()));
          },
        ),
      ),
    );
  }

  registerFields() => Container(
    child: Form(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          _tituloWidget(),
          _descripcionWidget(),
          _ubicacionWidget(),
          _referenciaWidget(),
          //_cargarUbicacion(),

          //Dropdown de categoría

          Row(
            children: <Widget>[
              SizedBox(width: 18.0),
              Icon(Icons.category),
              SizedBox(width: 14.0,),
              Expanded(
                child: new DropdownButton(
                  value: _categoriaSelect,
                  items: getCategorias().toList(),
                  hint: Text('Selecciona una categoría'),
                  onChanged: (newVal){
                    setState(() {
                      _categoriaSelect = newVal;
                      _idCat = _categoriaSelect;
                    });
                  },
                ),
              ),
            ],
          ),

          //Dropdown de método de pago

          //_confirmPasswordWidget(),
          SizedBox(height:size.getWidthPx(80)),
          _aplicarButtonWidget(),
        ],
      )
    ),
  );


  //Método get para la consulta a la API, intento de igual la consulta como de los trabajos

  Future<List<Categoria>> _cargarCategoria() async {

    final resp = await http.get('http://workio.somee.com/api/CategoriaTrabajos', headers: {"content-Type": "application/json"});
    print(resp.statusCode);
    print(resp.body);

    
    if(resp.statusCode == 200){
      var jsonData = jsonDecode(resp.body);

      for(var item in jsonData){
        Categoria categoria = Categoria.fromJson(item);
        categorias.add(categoria);
        print(categoria.descripcion);
      }

      print("Cantidad de categorias: "+categorias.length.toString());
      setState(() {
        this.categorias = categorias;
      });
      return categorias;
    }else{
      return null;
    }
  }

  void mostrarAlerta(BuildContext context){
    showDialog(
      context: context,
      builder: (context){
        return  AlertDialog(
          title: Text("Publicación realizada"),
          content: Text("¡Acción realizada con éxito!"),
          actions: <Widget>[
            FlatButton(
              child: Text('OK'),
              onPressed: () => Navigator.push(context,
                MaterialPageRoute(
                  builder: (context)=> MisPublicacionesPage(widget.idPersona, widget.idPerfil)
                )
              ),
            )
          ],
        );
      }
    );
  }


}