import 'package:flutter/material.dart';
import 'package:workio_flutter/utils/colors.dart';
import 'package:workio_flutter/utils/responsive_screen.dart';
import 'package:workio_flutter/widgets/bottom_curve_painter.dart';
import 'package:workio_flutter/widgets/gradient_text.dart';

class AplicarTrabPage extends StatefulWidget {
  
  final Color focusBorderColor;

  const AplicarTrabPage({Key key, this.focusBorderColor}) : super(key: key);

  @override
  _AplicarTrabPageState createState() => _AplicarTrabPageState();
}

class _AplicarTrabPageState extends State<AplicarTrabPage> {
  
  
  Screen size;
  double widht;
  double height;

  TextEditingController _descripcionController = TextEditingController();

  @override
  void dispose(){
    super.dispose();
    _descripcionController.dispose();
  }
  

  @override
  Widget build(BuildContext context) {
    size = Screen(MediaQuery.of(context).size);
    height = MediaQuery.of(context).size.height;
    widht = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: backgroundColor,
      resizeToAvoidBottomInset: true,
      body: Stack(children: <Widget>[
        ClipPath(
          clipper: BottomShapeClipper(),
          child: Container(
            color: colorCurve,
          )
        ),
        SingleChildScrollView(
          child: SafeArea(
            top: true,
            bottom: false,
            child: Container(
              margin: EdgeInsets.symmetric(
                horizontal: size.getWidthPx(20),
                vertical: size.getWidthPx(20)
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _signUpGradientText(),
                  SizedBox(height: size.getWidthPx(25)),
                  _descripController(),
                  SizedBox(height: size.getWidthPx(240),),
                  _aceptarButtonWidget()
                ]
              ),
            ),
          ),
        )
      ])
    );
  }

  GradientText _signUpGradientText() {
    return GradientText('Aplicar al trabajo',
        gradient: LinearGradient(colors: [Colors.blue[600], Colors.teal[300]]),
        style: TextStyle(
            fontFamily: 'Exo2', fontSize: 25, fontWeight: FontWeight.bold));
  }

  Widget _descripController(){
    widht = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;
    size = Screen(MediaQuery.of(context).size);
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Container(
              margin: EdgeInsets.only(top: height / 400),
              padding: EdgeInsets.all(size.getWidthPx(0)),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Colors.grey.shade100,
                border: Border.all(color: widget.focusBorderColor??Colors.grey.shade400, width: 1.0),
                borderRadius: BorderRadius.circular(8.0),
              ),
              child: TextField(
                maxLines: null,
                obscureText: false,
                controller: _descripcionController,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: 'Descripción de la solución',
                  prefixIcon: Icon(Icons.edit,
                  color: colorIcons, 
                  size: size.getWidthPx(22)
                ),
              ),
            ),
          )),
        ],
      ),
      padding: EdgeInsets.only(bottom :size.getWidthPx(8)),
      margin: EdgeInsets.only(top: size.getWidthPx(8), right: size.getWidthPx(8), left:size.getWidthPx(8)),
    );
  }

  Widget _aceptarButtonWidget() {
    return Center(
      child: Container(
        
        padding: EdgeInsets.symmetric(
          vertical: size.getWidthPx(20), horizontal: size.getWidthPx(16)
        ),
        width: size.getWidthPx(200),
        child: RaisedButton(
          elevation: 3.0,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
          padding: EdgeInsets.all(size.getWidthPx(12)),
          child: Text(
            "ACEPTAR",
            style: TextStyle(
                fontFamily: 'Exo2', color: Colors.white, fontSize: 20.0),
          ),
          color: Colors.teal,
          onPressed: ()  {
            // Going to DashBoard
            //Navigator.push(context, MaterialPageRoute(builder: (context)=>HomePage()));
          },
        ),
      ),
    );
  }

}