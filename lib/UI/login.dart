import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:workio_flutter/BO/usuario.dart';
import 'package:workio_flutter/StaticData/UsuarioLogged.dart';
import 'package:workio_flutter/UI/home_page.dart';
import 'package:workio_flutter/src/asset.dart';
import 'package:workio_flutter/src/base_dialog.dart';

// import 'package:workio_flutter/UI/home_page.dart';
// import 'package:workio_flutter/main.dart';
// import 'package:workio_flutter/routes/routes.dart';

class LoginPage extends StatelessWidget {
  TextEditingController _userTexField = TextEditingController();
  TextEditingController _passTextField = TextEditingController();

  void dispose() {
    _userTexField.dispose();
    _passTextField.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
        image: AssetImage('assets/screen.jpg'),
        fit: BoxFit.cover,
      )),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Container(
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.transparent,
          ),
          child: Padding(
            padding: EdgeInsets.all(23),
            child: ListView(
              children: <Widget>[
                SizedBox(
                  height: 90,
                ),
                Form(
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
                        child: TextFormField(
                          controller: _userTexField,
                          style: TextStyle(
                            color: Colors.white,
                          ),
                          decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white)),
                              labelText: 'Nombre de usuario',
                              icon: Icon(
                                Icons.person_outline,
                                color: Colors.white,
                              ),
                              labelStyle:
                                  TextStyle(fontSize: 15, color: Colors.white)),
                        ),
                      ),
                      TextFormField(
                        controller: _passTextField,
                        obscureText: true,
                        style: TextStyle(
                          color: Colors.white,
                        ),
                        decoration: InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white)),
                            labelText: 'Contraseña',
                            icon: Icon(
                              Icons.lock_outline,
                              color: Colors.white,
                            ),
                            labelStyle:
                                TextStyle(fontSize: 15, color: Colors.white)),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20, bottom: 5),
                  child: Text(
                    '¿Olvidaste tu contraseña?',
                    textAlign: TextAlign.right,
                    style: TextStyle(
                        fontFamily: 'SFUIDisplay',
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20),
                  child: MaterialButton(
                    onPressed: () {
                      _login(_userTexField.text, _passTextField.text, context);
                    },
                    child: Text(
                      'INICIAR SESIÓN',
                      style: TextStyle(
                          fontSize: 15,
                          fontFamily: 'SFUIDisplay',
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                    color: Color(0xffff2d55),
                    elevation: 0,
                    minWidth: 350,
                    height: 60,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50)),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 30),
                  child: Center(
                    child: RichText(
                      text: TextSpan(children: [
                        TextSpan(
                            text: "¿No tienes una cuenta?",
                            style: TextStyle(
                              fontFamily: 'SFUIDisplay',
                              color: Colors.white,
                              fontSize: 15,
                            )),
                      ]),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20),
                  child: MaterialButton(
                    onPressed: () {
                      showDialog(
                          context: context,
                          builder: (_) => AssetGiffyDialog(
                                // key: key[3],
                                image: Image.asset(
                                  'assets/loginIMG.jpg',
                                  fit: BoxFit.cover,
                                ),
                                title: Text(
                                  'Registrate como...',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 22.0,
                                      fontWeight: FontWeight.w600),
                                ),
                                entryAnimation: EntryAnimation.DEFAULT,
                                onOkButtonPressed: () {},
                              ));
                    },
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Text(
                          'REGISTRATE',
                          style: TextStyle(
                              fontSize: 15,
                              fontFamily: 'SFUIDisplay',
                              color: Colors.black),
                        )
                      ],
                    ),
                    color: Colors.transparent,
                    elevation: 0,
                    minWidth: 350,
                    height: 60,
                    textColor: Colors.white,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50),
                        side: BorderSide(color: Colors.black)),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

Future _login(String user, String pass, BuildContext context) async {
  final resp = await http
      .get('http://workio.somee.com/api/Login?username=$user&pass=$pass');
  print(resp.statusCode);

  if (resp.statusCode == 200) {
    Map usuarioMap = jsonDecode(resp.body);
    var usuario = Usuario.fromJson(usuarioMap);

    print('El usuario es: ${usuario.username}');
    print('El usuario es: ${usuario.email}');

    UserLogged.usuario = usuario;

    if (usuario.estatusId == 2) {
      mostrarAlertaStatus(context);
    }
    if (usuario.estatusId == 1) {
      print(resp.body.toString());
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => HomePage(usuario.personaId, usuario.perfilId)));
    }
  } else {
    mostrarAlerta(context);
  }
}

void mostrarAlertaStatus(BuildContext context) {
  showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('Autorización pendiente'),
          content: Text('Favor de subir la documentación pertinente'),
          actions: <Widget>[
            FlatButton(
              child: Text('OK'),
              onPressed: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) => LoginPage())),
            )
          ],
        );
      });
}

void mostrarAlerta(BuildContext context) {
  showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('Información incorrecta'),
          content: Text(
              'El nombre de usuario o la contraseña ingresada son incorrectas.'),
          actions: <Widget>[
            FlatButton(
              child: Text('OK'),
              onPressed: () => Navigator.of(context).pop(),
            )
          ],
        );
      });
}
