import 'package:flutter/material.dart';
import 'package:workio_flutter/widgets/gradient_text.dart';

class ChatPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: SafeArea(
        child: ListView(padding: EdgeInsets.all(10.0), 
          children: <Widget>[
            _trabajosGradientText(),
            SizedBox(height: 20),
          ]
        ),
      ), 
    );
  }

  GradientText _trabajosGradientText() {
    return GradientText('Chats',
        gradient: LinearGradient(colors: [Colors.blue[600], Colors.teal[300]]),
        style: TextStyle(
            fontFamily: 'Exo2', fontSize: 25, fontWeight: FontWeight.bold));
  }
}