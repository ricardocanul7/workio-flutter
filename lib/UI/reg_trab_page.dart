import 'dart:convert';
import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import 'package:workio_flutter/BO/persona.dart';
import 'package:workio_flutter/BO/usuario.dart';
import 'package:workio_flutter/UI/home_page.dart';
import 'package:workio_flutter/UI/login.dart';
import 'package:workio_flutter/utils/utils.dart';
import 'package:workio_flutter/widgets/foto_perfil.dart';
import 'package:workio_flutter/widgets/select_date.dart';
import 'package:workio_flutter/widgets/widgets.dart';

class SignUpTrabPage extends StatefulWidget {
  
  final Color focusBorderColor;

  const SignUpTrabPage({Key key, this.focusBorderColor}): super(key:key);
  
  @override
  _SignUpTrabPageState createState() => _SignUpTrabPageState();
}

class _SignUpTrabPageState extends State<SignUpTrabPage> {
  Persona _persona = Persona();
  Usuario _usuario = Usuario();

  File foto;

  TextEditingController _nameController = TextEditingController();
  TextEditingController _apellidoPController = TextEditingController();
  TextEditingController _apellidoMController = TextEditingController();
  TextEditingController _fechaNacimientoController = TextEditingController();
  TextEditingController _direccionController = TextEditingController();
  TextEditingController _telefonoController = TextEditingController();
  TextEditingController _celularController = TextEditingController();
  TextEditingController _usernameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _confirmPasswordController = TextEditingController();

  @override
  void dispose() {
    super.dispose();
    _nameController.dispose();
    _apellidoPController.dispose();
    _apellidoMController.dispose();
    _fechaNacimientoController.dispose();
    _direccionController.dispose();
    _telefonoController.dispose();
    _celularController.dispose();
    _usernameController.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    _confirmPasswordController.dispose();
  }

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Screen size;
  double widht;
  double height;

  @override
  Widget build(BuildContext context) {
    size = Screen(MediaQuery.of(context).size);
    height = MediaQuery.of(context).size.height;
    widht = MediaQuery.of(context).size.height;

    return Scaffold(
        backgroundColor: backgroundColor,
        resizeToAvoidBottomInset: true,
        body: Stack(children: <Widget>[
          ClipPath(
              clipper: BottomShapeClipper(),
              child: Container(
                color: colorCurve,
              )),
          SingleChildScrollView(
            child: SafeArea(
              top: true,
              bottom: false,
              child: Container(
                margin: EdgeInsets.symmetric(
                    horizontal: size.getWidthPx(20),
                    vertical: size.getWidthPx(20)),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          _signUpGradientText(),
                        ],
                      ),
                      SizedBox(height: size.getWidthPx(10)),
                      _textAccount(),
                      SizedBox(height: size.getWidthPx(30)),
                      registerFields()
                    ]),
              ),
            ),
          )
        ]));
  }

  Widget _botonFoto() {
    return Row(
      children: <Widget>[
        SizedBox(width: size.getWidthPx(70)),
        IconButton(
          icon: Icon(Icons.add_a_photo, color: colorIcons),
          onPressed: _tomarFoto,
        ),
        SizedBox(width: size.getWidthPx(90)),
        IconButton(
          icon: Icon(Icons.add_photo_alternate, color: colorIcons),
          onPressed: _seleccionarFoto,
        ),
      ],
    );
  }

  RichText _textAccount() {
    return RichText(
      text: TextSpan(
          text: "¿Ya te has registrado? ",
          children: [
            TextSpan(
              style: TextStyle(color: colorCurve),
              text: 'Inicia sesión aquí',
              recognizer: TapGestureRecognizer()
                ..onTap = () => Navigator.pop(context),
            )
          ],
          style: TextStyle(
              fontFamily: 'Exo2', color: Colors.black87, fontSize: 16)),
    );
  }

  GradientText _signUpGradientText() {
    return GradientText('Registrate como trabajador',
        gradient: LinearGradient(colors: [Colors.blue[600], Colors.teal[300]]),
        style: TextStyle(
            fontFamily: 'Exo2', fontSize: 25, fontWeight: FontWeight.bold));
  }

  FotoPerfilWidget _fotoPerfil(){
    return FotoPerfilWidget(foto);
  }

  BoxField _nameWidget() {
    return BoxField(
        controller: _nameController,
        hintText: "Nombre(s)",
        lableText: "Name",
        obscureText: false,
        icon: Icons.person,
        iconColor: colorIcons);
  }

  BoxField _apellidoPWidget() {
    return BoxField(
        controller: _apellidoPController,
        hintText: "Apellido Paterno",
        lableText: "apellidoP",
        obscureText: false,
        icon: Icons.person,
        iconColor: colorIcons);
  }

  BoxField _apellidoMWidget() {
    return BoxField(
        controller: _apellidoMController,
        hintText: "Apellido Materno",
        lableText: "apellidoM",
        obscureText: false,
        icon: Icons.person,
        iconColor: colorIcons);
  }

  SelectDateWidget _fechaNacWidget(){
    return SelectDateWidget();
  }

  BoxField _direccionWidget() {
    return BoxField(
        controller: _direccionController,
        hintText: "Direccion",
        lableText: "direccion",
        obscureText: false,
        icon: Icons.home,
        iconColor: colorIcons);
  }

  BoxField _telefonoWidget() {
    return BoxField(
        controller: _telefonoController,
        hintText: "Telefono",
        lableText: "telefono",
        obscureText: false,
        icon: Icons.call,
        iconColor: colorIcons);
  }

  BoxField _celularWidget() {
    return BoxField(
        controller: _celularController,
        hintText: "Celular",
        lableText: "celular",
        obscureText: false,
        icon: Icons.home,
        iconColor: colorIcons);
  }

  _registroPersona() async {
    _persona.nombres = _nameController.text;
    _persona.apellidoP = _apellidoPController.text;
    _persona.apellidoM = _apellidoMController.text;
    _persona.fechaNacimiento = _fechaNacimientoController.text;
    _persona.fotoUrl = await _uploadImg();
    _persona.direccion = _direccionController.text;
    _persona.telefono = _telefonoController.text;
    _persona.celular = _celularController.text;

    final resp = await http.post(
      'http://www.workio.somee.com/api/Personas',
      body: jsonEncode(_persona),
      headers: {"content-Type": "application/json"},
    );

    print("cuerpo de post personas: ${resp.body}");

    Map personaMap = jsonDecode(resp.body);
    var persona = Persona.fromJson(personaMap);

    print("Id de persona: ${persona.id}");
    print('${resp.statusCode}');

    if(resp.statusCode ==201){
      _registroUsuario(persona.id);
    }
  }

  BoxField _usernameWidget() {
    return BoxField(
      controller: _usernameController,
      hintText: "Nombre de usuario",
      lableText: "Username",
      obscureText: false,
      icon: Icons.people,
      iconColor: colorIcons,
    );
  }

  BoxField _emailWidget() {
    return BoxField(
        controller: _emailController,
        hintText: "Correo electrónico",
        lableText: "Email",
        obscureText: false,
        icon: Icons.email,
        iconColor: colorIcons);
  }

  BoxField _passwordWidget() {
    
    return BoxField(
        maxLength: 10,
        controller: _passwordController,
        hintText: "Contraseña",
        lableText: "Password",
        obscureText: true,
        icon: Icons.lock_outline,
        iconColor: colorIcons
        );
  }

  // BoxField _confirmPasswordWidget() {
  //   return BoxField(
  //       controller: _confirmPasswordController,
  //       hintText: "Confirmar contraseña",
  //       lableText: "Confirm Password",
  //       obscureText: true,
  //       icon: Icons.lock_outline,
  //       iconColor: colorIcons);
  // }

  _validarUsername() async {
    final resp = await http.get(
        'http://workio.somee.com/api/RegistroUsuario/UsernameExist?username=$_usernameController');
      //
    print(resp.body);

    if (resp.body == 'false') {
      return false;
    } else {
      return true;
    }
  }

  _registroUsuario(personaId) async {
    _usuario.username = _usernameController.text;
    _usuario.email = _emailController.text;
    _usuario.contrasenia = _passwordController.text;
    _usuario.fechaRegistro =  await _getDate();
    _usuario.estatusId = 2; //revision
    _usuario.personaId = personaId;
    _usuario.perfilId = 3; //trabajador

    print(jsonEncode(_usuario));

    if(_usuario.personaId==null){
      print('LA ESTÁS CAGANDO Y MUY QLERO');
    }
    else{
      print('TODO CHIDO KRNAL');
      print('EL ID PERSONA ES: ${_usuario.personaId}');
    }

    final resp = await http.post(
    //'http://www.workio.somee.com/api/Usuarios',
    'http://workio.somee.com/api/RegistroUsuario',
      body: jsonEncode(_usuario),
      headers: {"content-Type": "application/json"},
    );
    print("Codigo usuario: ${resp.statusCode}"); 
    print("cuerpo de post usuario: ${resp.body}");

    
    
    if (resp.statusCode == 201) {
      print("Registro de usuario exitoso");
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => HomePage(_usuario.personaId,_usuario.perfilId)));
      mostrarAlerta(context);
    }
  }

  Future<String> _getDate() async{
    String _today;
   _today = '${DateTime.now().year.toString()}-${DateTime.now().month.toString().padLeft(2, '0')}-${DateTime.now().day.toString().padLeft(2, '0')}';
   print('La fecha de hoy es: $_today');
    return _today;
  }



  Container _signUpButtonWidget() {
    return Container(
      padding: EdgeInsets.symmetric(
          vertical: size.getWidthPx(20), horizontal: size.getWidthPx(16)),
      width: size.getWidthPx(200),
      child: RaisedButton(
        elevation: 3.0,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
        padding: EdgeInsets.all(size.getWidthPx(12)),
        child: Text(
          "Registrarse",
          style: TextStyle(
              fontFamily: 'Exo2', color: Colors.white, fontSize: 20.0),
        ),
        color: Colors.teal,
        onPressed: () async {
          if (!(await _validarUsername())) {
            //_uploadImg();
            _registroPersona();
          }

          //_uploadImg();

          // Going to DashBoard
          //Navigator.push(context, MaterialPageRoute(builder: (context)=>HomePage()));
        },
      ),
    );
  }

  registerFields() => Container(
    child: Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          _fotoPerfil(),
          _botonFoto(),
          _nameWidget(),
          _apellidoPWidget(),
          _apellidoMWidget(),
          _fechaNacWidget(),
          _direccionWidget(),
          _telefonoWidget(),
          _celularWidget(),
          _usernameWidget(),
          _emailWidget(),
          _passwordWidget(),
          //_confirmPasswordWidget(),
          _signUpButtonWidget(),
        ],
      )
    ),
  );

  _seleccionarFoto() async {
    _procesarImagen(ImageSource.gallery);
  }

  _tomarFoto() async {
    _procesarImagen(ImageSource.camera);
  }

  _procesarImagen(ImageSource origen) async {
    foto = await ImagePicker.pickImage(source: origen);

    if (foto != null) {
      //Limpieza

    }
    setState(() {});
  }

  _uploadImg() async {
    String _url;

    //String username = _usernameController.text;

    StorageReference reference = FirebaseStorage.instance
        .ref()
        .child("foto_perfil/${_usernameController.text}" + ".jpg");
    StorageUploadTask uploadTask = reference.putFile(foto);
    StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;
    var downURL = await (taskSnapshot).ref.getDownloadURL();
    _url = downURL;
    print('La foto se subió');
    print(_url);
    return _url;
  }

  void mostrarAlerta(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('Autorización pendiente'),
          content: Text('Favor de subir la documentación pertinente'),
          actions: <Widget>[
            FlatButton(
              child: Text('OK'),
              onPressed: () => Navigator.push(context,
              MaterialPageRoute(builder: (context) => LoginPage())),
            )
          ],
        );
      }
    );
  }
}
