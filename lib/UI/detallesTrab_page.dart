import 'dart:convert';

import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';
import 'package:workio_flutter/BO/trabajo.dart';
import 'package:workio_flutter/UI/aplicantes_page.dart';
import 'package:workio_flutter/UI/aplicarTrab_page.dart';
import 'package:workio_flutter/utils/colors.dart';
import 'package:workio_flutter/utils/responsive_screen.dart';
import 'package:workio_flutter/widgets/bottom_curve_painter.dart';
import 'package:workio_flutter/widgets/gradient_text.dart';

class DetallesTrabPage extends StatefulWidget {
  
  final int idTrab;

  const DetallesTrabPage( this.idTrab);

  @override
  _DetallesTrabPageState createState() => _DetallesTrabPageState();
}

class _DetallesTrabPageState extends State<DetallesTrabPage> {

  
  Screen size;
  double widht;
  double height;

  @override
  Widget build(BuildContext context) {
    size = Screen(MediaQuery.of(context).size);
    height = MediaQuery.of(context).size.height;
    widht = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: backgroundColor,
      resizeToAvoidBottomInset: true,
      body: Stack(children: <Widget>[
        ClipPath(
          clipper: BottomShapeClipper(),
          child: Container(
            color: colorCurve,
          )
        ),
        SingleChildScrollView(
          child: SafeArea(
            top: true,
            bottom: false,
            child: Container(
              margin: EdgeInsets.symmetric(
                horizontal: size.getWidthPx(20),
                vertical: size.getWidthPx(20)
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      _signUpGradientText(),
                      SizedBox(width: 54,),
                      _opcionesDetalles(),
                    ],
                  ),
                  
                  SizedBox(height: size.getWidthPx(25)),
                  Container(
                    child: FutureBuilder(
                      future: _cargarDetalles(),
                      builder: (BuildContext context, AsyncSnapshot snapshot){
                        if(snapshot.data == null){
                          return Container(child: Center(child: Text("Cargando..."),),);
                        } else{
                          return  _detallesTrab(context, snapshot.data.titulo, snapshot.data.descripcion);
                        }
                      },
                    ),
                  ),
                  Padding(padding: EdgeInsets.all(90)),
                  _aplicarButtonWidget()
                ]
              ),
            ),
          ),
        )
      ])
    );
  }

  GradientText _signUpGradientText() {
    return GradientText('Detalles del trabajo',
        gradient: LinearGradient(colors: [Colors.blue[600], Colors.teal[300]]),
        style: TextStyle(
            fontFamily: 'Exo2', fontSize: 25, fontWeight: FontWeight.bold));
  }

  Widget _detallesTrab(BuildContext context, String titulo, String descripcion) {

    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: size.getWidthPx(20),
        vertical: size.getWidthPx(20)
      ),
      child: Column(
        children: <Widget>[
          ListTile(
            title: Text(titulo),
          ),
          SizedBox(height: size.getWidthPx(10)),
          ListTile(
            title: Text(descripcion),
          ),
        ],
      ),
    );
  }

  Widget _aplicarButtonWidget() {
    return Center(
      child: Container(
        
        padding: EdgeInsets.symmetric(
          vertical: size.getWidthPx(20), horizontal: size.getWidthPx(16)
        ),
        width: size.getWidthPx(200),
        child: RaisedButton(
          elevation: 3.0,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
          padding: EdgeInsets.all(size.getWidthPx(12)),
          child: Text(
            "APLICAR",
            style: TextStyle(
                fontFamily: 'Exo2', color: Colors.white, fontSize: 20.0),
          ),
          color: Colors.teal,
          onPressed: ()  {
            Navigator.push(context, 
              MaterialPageRoute(builder: (context)=> AplicarTrabPage()));
            // Going to DashBoard
            //Navigator.push(context, MaterialPageRoute(builder: (context)=>HomePage()));
          },
        ),
      ),
    );
  }

  Future _cargarDetalles() async{

    int idTrabajo = widget.idTrab;

    final resp = await http.get(
      'http://workio.somee.com/api/PublicacionTrabajos/$idTrabajo'
    );
 

    if(resp.statusCode==200){

      Map detallesMap = jsonDecode(resp.body);
      var trabajo = Trabajo.fromJson(detallesMap);

      print(resp.body);

      print('El titulo es: ${trabajo.titulo}');
      print('La descripción es: ${trabajo.descripcion}');
      return trabajo; 
    }else{
      return null;
    }
  }

  PopupMenuButton _opcionesDetalles(){
    return PopupMenuButton(
      itemBuilder: (BuildContext context){
        return [
          PopupMenuItem(
            child: InkWell(
              child: Text("Lista de aplicantes"),
              onTap: (){
                Navigator.push(context, 
                  MaterialPageRoute(builder: (context)=> AplicantesPage())
                );
              },
            ),
          )
        ];
      }
    );
  }

}
